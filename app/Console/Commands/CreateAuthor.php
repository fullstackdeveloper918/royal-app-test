<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class CreateAuthor extends Command
{
    protected $signature = 'make:author';
    protected $description = 'Create a new author and send data to external API';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()

    {

        $accessToken = env('ACCESS_TOKEN');

// Check if the access token is available
if (!$accessToken) {
    echo 'Access token not found.';
} else {
    echo 'Access token: ' . $accessToken;
}
        // Gather author details from the user
        $firstName = $this->ask('First name');
        $lastName = $this->ask('Last name');
        $birthdayInput = $this->ask('Birthday (YYYY-MM-DD)');
        $biography = $this->ask('Biography');
        $gender = $this->choice('Gender', ['male', 'female', 'other']);
        $placeOfBirth = $this->ask('Place of birth');

        // Convert birthday to the desired format
        try {
            $birthday = Carbon::createFromFormat('Y-m-d', $birthdayInput)->toIso8601String();
        } catch (\Exception $e) {
            $this->error('Invalid date format. Please use YYYY-MM-DD.');
            return;
        }

        // Save author locally (optional)
        

        // Prepare data for API request
        $authorData = [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'birthday' => $birthday,
            'biography' => $biography,
            'gender' => $gender,
            'place_of_birth' => $placeOfBirth,
        ];
        
       
        // Send POST request to the external API
        $response = Http::withToken($accessToken)
            ->withOptions(['verify' => false])
            ->post('https://candidate-testing.api.royal-apps.io/api/v2/authors', $authorData);

        if ($response->successful()) {
            $this->info('Author created successfully and data sent to external API!');
        } else {
            $this->error('Failed to send data to external API: ' . $response->body());
        }
    }
}
