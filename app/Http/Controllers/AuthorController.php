<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AuthorController extends Controller
{
    public function index()

    {

    
        $response = Http::withToken(session('access_token'))->withOptions(['verify' => false])
                        ->get('https://candidate-testing.api.royal-apps.io/api/v2/authors');

        $authors = $response->json()['items'];
        return view('authors.index', compact('authors'));
    }

    public function show($id)
    {
        $authorResponse = Http::withToken(session('access_token'))->withOptions(['verify' => false])
                              ->get("https://candidate-testing.api.royal-apps.io/api/v2/authors/{$id}");
        $author = $authorResponse->json();
        
        return view('authors.show', compact('author'));
    }

   public function destroy($id)
{
    // Ensure the author has no related books
    $booksResponse = Http::withToken(session('access_token'))->withOptions(['verify' => false])
    ->get("https://candidate-testing.api.royal-apps.io/api/v2/authors/{$id}");
    $books = $booksResponse->json()['books'];
    if (count($books) > 0) {
        return redirect()->back()->with('error', 'Author cannot be deleted because there are related books.');
    }

    // Proceed to delete the author if there are no books
    $response = Http::withToken(session('access_token'))->withOptions(['verify' => false])
                    ->delete("https://candidate-testing.api.royal-apps.io/api/v2/authors/{$id}");

    if ($response->successful()) {
        return redirect()->route('authors.index')->with('success', 'Author deleted successfully.');
    } else {
        return redirect()->back()->with('error', 'Failed to delete the author.');
    }
}
}
