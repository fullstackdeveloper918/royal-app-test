<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

use Session ;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $response = Http::withOptions(['verify' => false])->post('https://candidate-testing.api.royal-apps.io/api/v2/token', [
            'email' => $request->email,
            'password' => $request->password,
        ]);

        if ($response->successful()) {
            $data = $response->json();
            $token = $data['token_key'];
            Session::put('access_token', $token);
            // Store access token in .env
            file_put_contents(app()->environmentFilePath(), 'ACCESS_TOKEN=' . $token . PHP_EOL, FILE_APPEND | LOCK_EX);
            $user = $data['user'];
            Session::put('user_name', $user['first_name'] . ' ' . $user['last_name']);
            return redirect()->route('dashboard');
        } else {
            return back()->withErrors(['message' => 'Invalid credentials']);
        }
    }


    public function logout()
{
    // Remove access token from session
    session()->forget('access_token');
    // Remove access token from .env file
    $envFile = app()->environmentFilePath();
    $currentEnv = file_get_contents($envFile);
    $newEnv = preg_replace('/ACCESS_TOKEN=.*\n/', '', $currentEnv);
    file_put_contents($envFile, $newEnv);
    return redirect()->route('login');
}
}
