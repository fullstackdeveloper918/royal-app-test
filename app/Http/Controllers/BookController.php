<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DateTime;


class BookController extends Controller
{
    public function create()
    {
        $response = Http::withToken(session('access_token'))->withOptions(['verify' => false])
                        ->get('https://candidate-testing.api.royal-apps.io/api/v2/authors');
    
        $authors = $response->json()['items'];
        return view('books.create', compact('authors'));
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'author_id' => 'required|integer',
            'description' => 'required|string',
            'isbn' => 'required|string',
            'release_date' => 'required|date',
            'format' => 'required|string',
            'number_of_pages' => 'required|integer',
        ]); 

        $releaseDate = new DateTime($request->release_date);
        $formattedReleaseDate = $releaseDate->format('Y-m-d\TH:i:s.v\Z');
        $response = Http::withToken(session('access_token'))->withOptions(['verify' => false])
        ->post('https://candidate-testing.api.royal-apps.io/api/v2/books', [
            'author' => ['id' => $request->author_id],
            'title' => $request->title,
            'description' => $request->description,
            'isbn' => $request->isbn,
            'format' => $request->format,
            'number_of_pages' => (int)$request->number_of_pages,
            'release_date' => (string)$formattedReleaseDate,

        ]);

        if ($response->successful()) {
        return redirect()->route('authors.show', $request->author_id)->with('success', 'Book added successfully');
        } else {
        return back()->withErrors(['message' => 'Failed to add book']);
        }
    }
    
    public function destroy($id)
{
    $response = Http::withToken(session('access_token'))->withOptions(['verify' => false])
                    ->delete("https://candidate-testing.api.royal-apps.io/api/v2/books/{$id}");

    if ($response->successful()) {
        return back()->with('success', 'Book deleted successfully.');
    } else {
        return back()->with('error', 'Failed to delete the book. Please try again.');
    }
}
}

