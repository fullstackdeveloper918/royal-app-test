<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Candidate Testing App</title>
</head>
<body>
    @if(session('access_token'))
        <p>Welcome, {{ session('user_name') }}</p>
        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <button type="submit">Logout</button>
        </form>
    @endif
    @yield('content')
</body>
</html>
