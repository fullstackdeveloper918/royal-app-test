<header>
    <style>
     /* styles.css */

header {
    background-color: #333;
    color: #fff;
    padding: 10px 20px;
    display: flex;
    justify-content: space-between;
    align-items: center;
}

.logo {
    font-size: 24px;
    font-weight: bold;
}

.user-dropdown {
    position: relative;
}

.user-icon {
    cursor: pointer;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 120px;
    box-shadow: 0 2px 5px rgba(0,0,0,0.1);
    z-index: 1;
    right: 0;
}

.dropdown-content a {
    color: #333;
    padding: 10px;
    display: block;
    text-decoration: none;
}

.dropdown-content a:hover {
    background-color: #ddd;
}

.username {
    font-weight: bold;
    padding: 5px;
    border-bottom: 1px solid #ddd;
}

    </style>
    <div class="logo">ROYAL APP</div>
    <div class="user-dropdown">
        <div class="user-icon"><i class="fas fa-user"></i></div>
        <div class="dropdown-content">
            <span class="username">{{ session('user_name') }}</span>
            <form method="POST" action="{{ route('logout') }}">
            @csrf
            <button type="submit" class="logout-button">Logout</button>
        </form>
        </div>
    </div>
</header>   