<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Book</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        h1 {
            text-align: center;
            margin-top: 30px;
        }
        form {
            width: 80%;
            max-width: 600px;
            margin: 20px auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        input[type="text"],
        select,
        textarea,
        input[type="datetime-local"],
        input[type="number"] {
            width: 100%;
            padding: 10px;
            margin: 10px 0;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-sizing: border-box;
            font-size: 16px;
        }
        select {
            appearance: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            background-image: url('data:image/svg+xml;utf8,<svg fill="#666" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M7.406 9.594l4 4c.39.39 1.024.39 1.414 0l4-4c.39-.39.39-1.023 0-1.414-.39-.39-1.023-.39-1.414 0L12 11.586 8.82 8.406c-.39-.39-1.023-.39-1.414 0-.39.39-.39 1.023 0 1.414z"/></svg>');
            background-size: 12px;
            background-repeat: no-repeat;
            background-position: right 10px top 50%;
            padding-right: 30px;
        }
        textarea {
            resize: vertical;
        }
        button[type="submit"] {
            background-color: #4CAF50;
            color: #fff;
            border: none;
            padding: 15px 20px;
            border-radius: 5px;
            cursor: pointer;
            font-size: 16px;
            width: 100%;
            display: block;
            transition: background-color 0.3s ease;
        }
        button[type="submit"]:hover {
            background-color: #0056b3;
        }
        .error-msg {
            color: red;
            margin-top: 5px;
        }
    </style>
</head>
<body>
    <h1>Add Book</h1>
    <form method="POST" action="{{ route('books.store') }}">
        @csrf
        <input type="text" name="title" placeholder="Book Title" required>
        <select name="author_id" required>
            <option value="" disabled selected>Select Author</option>
            @foreach ($authors as $author)
                <option value="{{ $author['id'] }}">{{ $author['first_name'] }} {{ $author['last_name'] }}</option>
            @endforeach
        </select>
        <input type="datetime-local" id="release_date" name="release_date" placeholder="Release Date" required>
        <textarea name="description" placeholder="Description" required></textarea>
        <input type="text" name="isbn" placeholder="ISBN" required>
        <input type="text" name="format" placeholder="Format" required>
        <input type="number" name="number_of_pages" placeholder="Number of Pages" required>
        <button type="submit">Add Book</button>
    </form>
</body>
</html>
