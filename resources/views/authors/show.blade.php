<!DOCTYPE html>
<html>
<head>
    <title>Author Details</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 20px;
            color: #333;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
        }
        .header {
            text-align: center;
            margin-bottom: 30px;
        }
        .author-details {
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            padding: 30px;
            margin-bottom: 30px;
        }
        .author-details h2 {
            margin-bottom: 20px;
            color: #007bff;
        }
        .author-details p {
            margin: 10px 0;
        }
        .book-list {
            list-style-type: none;
            padding: 0;
        }
        .book-list li {
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            margin-bottom: 20px;
            padding: 20px;
            position: relative;
        }
        .book-title {
            font-size: 1.2em;
            margin-bottom: 10px;
            color: #333;
            display: flex;
            align-items: center;
        }
        .book-title i {
            margin-right: 10px;
            color: #007bff;
        }
        .book-details p {
            margin: 5px 0;
        }
        .delete-button {
            position: absolute;
            top: 10px;
            right: 10px;
            background-color: #dc3545;
            color: #fff;
            border: none;
            padding: 5px 10px;
            cursor: pointer;
            border-radius: 3px;
            transition: background-color 0.3s ease;
        }
        .delete-button:hover {
            background-color: #c82333;
        }
        .alert {
            padding: 10px;
            border-radius: 5px;
            margin-bottom: 20px;
        }
        .alert-success {
            background-color: #d4edda;
            color: #155724;
        }
        .alert-error {
            background-color: #f8d7da;
            color: #721c24;
        }
    </style>
</head>
<body>

    <div class="container">
        <div class="header">
            <h1>{{ $author['first_name'] }} {{ $author['last_name'] }}</h1>
            <i class="fas fa-user"></i>
        </div>
        <div class="author-details">
            <h2>Author Information</h2>
            <p><strong>Birthday:</strong> {{ \Carbon\Carbon::parse($author['birthday'])->format('F d, Y') }}</p>
            <p><strong>Place of Birth:</strong> {{ $author['place_of_birth'] }}</p>
            <p><strong>Gender:</strong> {{ ucfirst($author['gender']) }}</p>
            <p><strong>Biography:</strong> {{ $author['biography'] }}</p>
        </div>

        @if(session('success'))
            <div class="alert alert-success" id="success-alert">
                {{ session('success') }}
            </div>
        @endif

        @if(session('error'))
            <div class="alert alert-error" id="error-alert">
                {{ session('error') }}
            </div>
        @endif

        <h2>Books</h2>
        <ul class="book-list">
            @foreach ($author['books'] as $book)
                <li>
                    <div class="book-title">
                        <i class="fas fa-book"></i>
                        {{ $book['title'] }}
                    </div>
                    <div class="book-details">
            <p><strong>Description:</strong> {{ $book['description'] }}</p>
            <p><strong>ISBN:</strong> {{ $book['isbn'] }}</p>
            <p><strong>Format:</strong> {{ $book['format'] }}</p>
            <p><strong>Number of Pages:</strong> {{ $book['number_of_pages'] }}</p>
            @if (!empty($book['release_date']))
                <p><strong>Release Date:</strong> {{ \Carbon\Carbon::parse($book['release_date'])->format('F d, Y') }}</p>
            @endif
        </div>
                    <form action="{{ route('books.destroy', $book['id']) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="delete-button">Delete</button>
                    </form>
                </li>
            @endforeach
        </ul>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            setTimeout(function () {
                let successAlert = document.getElementById('success-alert');
                if (successAlert) {
                    successAlert.style.display = 'none';
                }
                let errorAlert = document.getElementById('error-alert');
                if (errorAlert) {
                    errorAlert.style.display = 'none';
                }
            }, 5000);
        });
    </script>
</body>
</html>
